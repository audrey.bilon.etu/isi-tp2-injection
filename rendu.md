# Rendu "Injection"

## Monome

Nom, Prénom, email: BILON, Audrey, audrey.bilon.etu@univ-lille.fr 

## Commande importante
- Se connecter sur le serveur de la fac:
```
ssh ubuntu@[adresseIP]
```
- Activation de l'environnement : 
```
source <chemin vers votre environnement virtuel>/bin/activate
```
ici le dossier est isi-tp2-environnement

* Depuis l'environnement :
- Connecter à  base de données MySQL:
```
mysql -p -u isitp2 isitp2
```
- Utiliser une base de données:
```
use isitp2;
```
- Lancer le serveur :
```
./serveur.py
```

* Depuis un autre terminal :
- Accès à la page web:
```
 ssh ubuntu@172.28.101.192 -L 8080:127.0.0.1:8080
```
- Site web:
[http://localhost:8080]



## Question 1

* Quel est ce mécanisme? 
Le mécanisme consiste à utiliser un langage régulier qui permet le contrôle des mots entrés par l'utilisateur.
Ces derniers ne peuvent être composé uniquement de lettres et de chiffres. 

* Est-il efficace? Pourquoi? 
Ainsi la création de nouvelles requêtes est impossible (car l'espace n'est pas un caractère acceptant)
et les égalités aussi (on n'aura pas accès à toute la table) car les signes ne le sont pas non plus. 
Cependant, cela ne limite pas l'accès aux champs auquels nous ne voulons pas que l'utilisateur ait accès.

## Question 2

* Votre commande curl
curl 'http://127.0.0.1:8080/' --data-raw  chaine='1=1'&submit=OK
Pour voir les variables à compléter, nous avons besoin des champs à compléter.
Pour les voir, nous faisons: 
- un click droit sur la page 
- inspecter
- Onglet Network -> Payloads (pour voir les variables de la requête)


## Question 3

* Votre commande curl qui va permettre de rajouter une entree en mettant un contenu arbutraire dans le champ 'who'
curl 'http://127.0.0.1:8080/' --data-raw  chaine='text%27%2C%27aaa%27%29%23'&submit=OK 
Ici nous écrivons une requête permettant d'ignorer la fin de la requête SQL python.
% permet d'utiliser sous forme de code ascii hexadécimale les caractères qui ne sont pas interprétable sur le terminal: ',),(.
Le résultat: text envoye par: aaa

* Expliquez comment obtenir des informations sur une autre table
Pour cela nous pouvons sélectionner l'intégralité de l'autre table en faisant un select all avec le nom de cette dernière.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.
Pour que l'injection python soit totalement prise en compte, nous devons utiliser une variable intermédiaire, qui est un tuple.
Nous devons aussi utiliser une chaine de caractère permettant de remplacé les éléments du tuple aux %s (fonctionnement équivalent à printf)
Le résultat: text','aaa')# envoye par: 127.0.0.1

## Question 5

* Commande curl pour afficher une fenetre de dialog. 
curl 'http://127.0.0.1:8080/' --data-raw  chaine='<script>alert("HELLO!")</script>'&submit=OK

* Commande curl pour lire les cookies


## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.
Ce traitement est à réaliser dans le join, car c'est là que la chaine comportant la balise sera concaténée au code html.
escape du module html permet d'éviter de lire les balises de la chaine litéralement. 


